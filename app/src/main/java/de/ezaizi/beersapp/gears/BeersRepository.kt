package de.ezaizi.beersapp.gears

import de.ezaizi.beersapp.model.Beer
import de.ezaizi.beersapp.network.ApiProvider
import de.ezaizi.beersapp.network.BeerApi
import de.ezaizi.beersapp.network.dto.BeerDto
import de.ezaizi.beersapp.utils.BeerApiListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class BeersRepository @Inject constructor() {

    private val beersApi =
        ApiProvider(
            api = BeerApi::class.java,
            baseUrl = "api.punkapi.com/v2/",
            loggingEnabled = true
        ).get()

    var beersList = ArrayList<BeerDto>()

    fun tryToGetBeersFromApi(

        beerApiListener: BeerApiListener

    ) {
        beersApi.getAllBeers().enqueue(object : Callback<List<BeerDto>> {
            override fun onFailure(call: Call<List<BeerDto>>, t: Throwable) {
                beerApiListener.onFailed()
            }

            override fun onResponse(
                call: Call<List<BeerDto>>,
                response: Response<List<BeerDto>>
            ) {
                if (response.isSuccessful) {
                    beersList = response.body() as ArrayList<BeerDto>
                    val beers = ArrayList<Beer>()
                    for (beer in beersList) {
                        beers.add(beer.convertBeerDtoToBeer())
                    }
                    beerApiListener.onSuccess(beers)
                }
            }
        })
    }
}