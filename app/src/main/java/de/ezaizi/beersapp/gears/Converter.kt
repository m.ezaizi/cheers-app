package de.ezaizi.beersapp.gears

import de.ezaizi.beersapp.model.Beer
import de.ezaizi.beersapp.network.dto.BeerDto

fun BeerDto.convertBeerDtoToBeer() =
    Beer(this.id, this.name, this.tagline, this.firstBrewed, this.description, this.imageUrl)