package de.ezaizi.beersapp.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import de.ezaizi.beersapp.R
import de.ezaizi.beersapp.ui.beerlist.BeerListFragment

class MainActivity : AppCompatActivity() {
    /*
     TODO REFACTORING!
        NO USING FROM DATABINDING, ANDROID NAVIGATION, NAV_GRAPH OR SAFE_ARGS
        BECAUSE OF SDK,JDK UPGRADE PROBLEM AND INCOMPATIBILITY OF SOME DEPENDENCIES
    */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, BeerListFragment.newInstance())
            .commitNow()

    }
}