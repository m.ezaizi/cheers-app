package de.ezaizi.beersapp.network


import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

const val HTTP_PROTOCOL = "https:"

class ApiProvider<T>(
    private val api: Class<T>,
    private val baseUrl: String?,
    private val loggingEnabled: Boolean

) : javax.inject.Provider<T> {


    private val retrofit by lazy {
        val okhttpClientBuilder = OkHttpClient.Builder()

        if (loggingEnabled) {

            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okhttpClientBuilder.addInterceptor(httpLoggingInterceptor)
            okhttpClientBuilder.addNetworkInterceptor(StethoInterceptor())
                .retryOnConnectionFailure(true)

        }
        Retrofit.Builder()
            .client(okhttpClientBuilder.build())
            .baseUrl("$HTTP_PROTOCOL$baseUrl")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    override fun get(): T = retrofit.create(api)
}