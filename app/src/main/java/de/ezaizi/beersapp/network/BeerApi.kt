package de.ezaizi.beersapp.network

import de.ezaizi.beersapp.network.dto.BeerDto
import retrofit2.Call
import retrofit2.http.GET

interface BeerApi {

    @GET("beers")
    fun getAllBeers(): Call<List<BeerDto>>
}