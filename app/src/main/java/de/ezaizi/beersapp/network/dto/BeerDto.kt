package de.ezaizi.beersapp.network.dto

import com.google.gson.annotations.SerializedName

class BeerDto(
    var id: String,
    var name: String,
    var tagline: String,
    @SerializedName("first_brewed")
    var firstBrewed: String,
    var description: String,
    @SerializedName("image_url")
    var imageUrl: String
)