package de.ezaizi.beersapp.ui.beerdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import coil.api.load
import de.ezaizi.beersapp.R
import de.ezaizi.beersapp.model.Beer
import de.ezaizi.beersapp.ui.beerlist.DETAILS_FRAGMENT_TAG

class BeerDetailsFragment : Fragment() {

    private lateinit var viewModel: BeerDetailsViewModel

    private var beer: Beer? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.beer_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[BeerDetailsViewModel::class.java]
        init()
    }

    private fun init() {
        setUpToolBar()
        beer = arguments?.getParcelable(DETAILS_FRAGMENT_TAG)
        view?.findViewById<TextView>(R.id.beer_name)?.text = beer?.name
        view?.findViewById<TextView>(R.id.beer_description)?.text = beer?.description
        view?.findViewById<ImageView>(R.id.beer_logo)?.load(beer?.imageUrl)
    }

    private fun setUpToolBar() =
        view?.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)?.apply {
            setNavigationIcon(R.drawable.ic_arrow_back_24)
            setOnClickListener {
                activity?.onBackPressed()
            }
        }
}