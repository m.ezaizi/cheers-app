package de.ezaizi.beersapp.ui.beerlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import de.ezaizi.beersapp.R
import de.ezaizi.beersapp.model.Beer
import de.ezaizi.beersapp.ui.beerdetails.BeerDetailsFragment
import de.ezaizi.beersapp.utils.BeerListAdapter

const val DETAILS_FRAGMENT_TAG = "DETAILS_FRAGMENT_TAG"

class BeerListFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun newInstance() = BeerListFragment()
    }

    private lateinit var viewModel: BeerListViewModel
    private var recyclerView: RecyclerView? = null
    private var loadingProgress: ProgressBar? = null
    private var refreshLayout: SwipeRefreshLayout? = null
    private var emptyView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.beer_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        startSyncAndObserveData()
    }

    private fun init() {
        viewModel = ViewModelProvider(this)[BeerListViewModel::class.java]
        refreshLayout = view?.findViewById(R.id.swipe_refresh)
        refreshLayout?.setOnRefreshListener(this)
        recyclerView = view?.findViewById(R.id.list)
        loadingProgress = view?.findViewById(R.id.loading_progressbar)
        emptyView = view?.findViewById(R.id.empty_view)
    }

    private fun startSyncAndObserveData() {
        observeData()
        viewModel.startSync()
    }

    private fun observeData() {
        viewModel.loading.observeForever {
            loadingProgress?.isIndeterminate = it
            loadingProgress?.isVisible = it
            refreshLayout?.isRefreshing = it
            observeListAndNotifyAdapter()
        }
    }

    private fun observeListAndNotifyAdapter() {
        viewModel.allBeers.observeForever { list ->
            emptyView?.isVisible = !(viewModel.loading.value ?: false) && list.isEmpty()
            recyclerView?.adapter =
                BeerListAdapter(list) { beer ->
                    navigateToBeerDetailsFragment(beer)

                }
            view?.findViewById<RecyclerView>(R.id.list)?.adapter?.notifyDataSetChanged()
        }
    }

    private fun passedInstance(beer: Beer) = BeerDetailsFragment().apply {
        arguments = Bundle().apply {
            putParcelable(DETAILS_FRAGMENT_TAG, beer)
        }
    }

    private fun navigateToBeerDetailsFragment(beer: Beer) =
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.container, passedInstance(beer))?.addToBackStack(DETAILS_FRAGMENT_TAG)
            ?.commit()

    override fun onRefresh() = startSyncAndObserveData()
}