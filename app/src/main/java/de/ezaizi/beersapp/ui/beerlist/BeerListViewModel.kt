package de.ezaizi.beersapp.ui.beerlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.ezaizi.beersapp.gears.BeersRepository
import de.ezaizi.beersapp.model.Beer
import de.ezaizi.beersapp.utils.BeerApiListener

class BeerListViewModel : ViewModel(), BeerApiListener {

    private val repository: BeersRepository by lazy {
        BeersRepository()
    }

    val loading = MutableLiveData(true)

    val allBeers = MutableLiveData<List<Beer>>(emptyList())

    override fun startSync() {
        loading.value = true
        repository.tryToGetBeersFromApi(this)
    }

    override fun onFailed() {
        loading.value = false
        allBeers.value = emptyList()
    }

    override fun onSuccess(beers: List<Beer>) {
        loading.value = false
        allBeers.value = beers
    }
}