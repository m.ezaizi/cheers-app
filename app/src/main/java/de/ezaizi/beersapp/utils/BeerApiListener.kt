package de.ezaizi.beersapp.utils

import de.ezaizi.beersapp.model.Beer

interface BeerApiListener {
    fun startSync()
    fun onFailed()
    fun onSuccess(beers: List<Beer>)
}