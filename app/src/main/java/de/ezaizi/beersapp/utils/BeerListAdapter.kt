package de.ezaizi.beersapp.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Space
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import de.ezaizi.beersapp.R
import de.ezaizi.beersapp.model.Beer


class BeerListAdapter(val beerList: List<Beer>,val onClick: (Beer) -> Unit) :
    RecyclerView.Adapter<BeerListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val beerName = itemView.findViewById<TextView>(R.id.beer_name)
        val endSpacing = itemView.findViewById<Space>(R.id.space)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerListAdapter.ViewHolder {
        val context: Context = parent.context
        val inflater = LayoutInflater.from(context)
        val beerItem: View = inflater.inflate(R.layout.item_beer, parent, false)

        return ViewHolder(beerItem)
    }

    override fun getItemCount(): Int {
        return beerList.count()
    }

    override fun onBindViewHolder(holder: BeerListAdapter.ViewHolder, position: Int) {
        val beer = beerList[position]
        holder.beerName.text = beer.name
        holder.endSpacing.isVisible = beerList[position] == beerList.last()
        onBeerClicked(holder, beer)
    }

    private fun onBeerClicked(
        holder: ViewHolder,
        beer: Beer
    ) {
        holder.itemView.setOnClickListener {
            onClick.invoke(beer)
        }
    }
}
